#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <string>
#include <iostream>
#include <fstream>
#include <chrono>
#include <iomanip>

namespace Bojan
{
	static long long serijski_broj = 0;
	class Ulazna_potvrda
	{


		//std::string kategorija_vozila;
		std::time_t vrijeme;		// Pomocna varijabla za dohvatanje trenutnog vremena
		std::tm* vrijeme_tm;		//Datum ulaska na autoput
		std::time_t vrijeme_ulaska_sek;
		std::ofstream datoteka;
		short int  ulazna_dionica;
		std::string kategorija;
		void unos(std::string &,short int &); //pomocna funkcija za unos kategorije i ulazne dionice
		std::fstream datoteka_serijski_b;//pomocna varijabla za serijski broj
	public:
		void napravi_potvrdu();
	};
}
