#include "Ulazna_potvrda.h"
#define BROJ_DIONICA 7

void Bojan::Ulazna_potvrda::unos(std::string& kategorija, short int& dionica)
{
	std::string pomocna = "0";
	int pom = -1;
	std::cout << "Kategorija(A,B,C,D,E): ";
	do
	{
		if (pomocna != "0")
		{
			std::cout << "Nepravilan unos!Pokusajte ponovo." << std::endl;;
			std::cout << "Kategorija(A,B,C,D,E): ";
		} //Uslov da se izbjegne prvo ispisivanje
		std::cin >> pomocna;
	} while (pomocna != "A" && pomocna != "B" && pomocna != "C" && pomocna != "D"  && pomocna != "E");//Zahtjevanje pravilnog unosa
	kategorija = pomocna;
	std::cout << "Dionica: ";
	do
	{
		if (pom != -1)
		{
			std::cout << "Nepravilan unos!Pokusajte ponovo." << std::endl;
			std::cout << "Dionica: ";
		}//Uslov da se izbjegne prvo ispisivanje
		std::cin >> pom;
	} while (pom < 1 || pom > BROJ_DIONICA); //Zahtjevanje pravilnog unosa
	dionica = pom;
	datoteka_serijski_b.open("serijski_broj.txt", std::ios_base::in);//otvaranje datoteke za citanje serijskog broja	
	datoteka_serijski_b.seekg(-4, std::ios_base::end);//pozicioniranje za jedan podatak unazad sa kraja datoteke
	datoteka_serijski_b >> serijski_broj;//citanje serijskog podatka iz datoteke
	datoteka_serijski_b.close();

}

void Bojan::Ulazna_potvrda::napravi_potvrdu()
{
	unos(kategorija, ulazna_dionica);
	datoteka.open( std::to_string(serijski_broj)+ ".txt", std::ios_base::out);//otvaranje datoteke za upis
	if (datoteka.is_open())
	{
		vrijeme = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()); // Trenutno vrijeme
		datoteka << "===============\n  Asphalt 110\n===============\n";
		datoteka <<std::right<< std::setfill('0')<< std::setw(15) << serijski_broj << std::endl;
		datoteka <<"===============\n";
		datoteka << "Kategorija:" << std::right<<std::setfill(' ')<<std::setw(4)<<kategorija << std::endl;
		datoteka << "Dionica:" << std::right << std::setfill(' ') << std::setw(5)<< ulazna_dionica << "-" << std::endl;
		datoteka << "===============\n";
		vrijeme_tm = std::localtime(&vrijeme); // Lokalno vrijeme
		vrijeme_ulaska_sek = std::time(NULL);
		datoteka << std::put_time(vrijeme_tm, "       %d.%m.%y\n       %T") << "\n===============\n";
		serijski_broj++;
		datoteka_serijski_b.open("serijski_broj.txt",std::ios_base::out | std::ios_base::app);
		datoteka_serijski_b <<serijski_broj << std::endl;//vracanje serijskog broja u datoteku za pravljenje sledece potvrde ako se ponovo pokrene program
		datoteka_serijski_b.close();
		datoteka.close();
	}

}
