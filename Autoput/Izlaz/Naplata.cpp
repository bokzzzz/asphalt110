#include "Naplata.h"

Zeljko::Naplata::Naplata()
{
	std::ifstream config("config.dll");
	int a, b;
	char c;
	float cijena;
	// Citanje cijena iz config datoteke
	while(true)
	{
		config >> c >> a >> b >> cijena;
		if (config.eof())
			break;
		cijene.push_back(std::make_tuple(c, a, b, cijena));
	}
}

void Zeljko::Naplata::izvrsiNaplatu(int izlaz, int broj_ul_p)
{
	cvor_s = std::to_string(broj_ul_p);
	cvor_s += ".txt";
	cvor.open(cvor_s); // Otvaranje potvrde
	int ulaz;
	cvor.ignore(150, ':');
	cvor >> std::ws >> kategorija; // Ucitavanje kategorije vozila
	cvor.ignore(50, ':');
	cvor >> ulaz; // Ucitavanje cvora na kojem je automobil usao
	cvor.close();
	std::remove(cvor_s.c_str()); // Obrisi datoteku nakon ucitavanja
	int min = ulaz < izlaz ? ulaz : izlaz; // Sortiranje jer su cijene u obliku:
	int max = ulaz > izlaz ? ulaz : izlaz; // (kategorija, min, max, cijena)
	for (auto& x : cijene)
		if (std::get<0>(x) == kategorija && std::get<1>(x) == min && std::get<2>(x) == max)
			cijena = std::get<3>(x); // Pronadji cijenu za koristenu dionicu
	racun.izdaj_Racun(ulaz, izlaz, cijena); // Izdaj racun
}
