#define _CRT_SECURE_NO_WARNINGS
#include "Obracun_troskova.h"
#include "Naplata.h"
#include <fstream>
#include <vector>
#include <iostream>
#include <string>
#include <conio.h>

bool error(const char* file)
{
	return std::ifstream(file).fail();
}

int main()
{
	if (error("login.dll") || error("config.dll"))
	{
		std::cout << "Nije moguce pokrenuti aplikaciju.";
		std::cin.ignore();
		std::quick_exit(-1);
	}
	std::ifstream login("login.dll");
	std::vector<std::tuple<std::string, std::string, std::string>> korisnici;
	std::string ime, prezime, lozinka;
	while (true)
	{
		login >> ime >> prezime >> lozinka;
		if (login.eof())
			break;
		korisnici.push_back(std::make_tuple(ime, prezime, lozinka));
	}
	std::cout << "Prijavite se na sistem...\n";
	do
	{
		std::cout << "Ime: ";
		std::cin >> ime;
		std::cout << "Prezime: ";
		std::cin >> prezime;
		for (auto& x : korisnici)
		{
			if (std::get<0>(x) == ime && std::get<1>(x) == prezime)
			{
				do
				{
					char input;
					std::cout << "Unesite lozinku: ";
					lozinka.clear();
					do
					{
						input = _getch();
						if (input == 13)
						{
							std::cout << '\n';
							break;
						}
						lozinka += input;
						std::cout << '*';
					} while (true);
				} while (lozinka != std::get<2>(x));
				Zeljko::korisnik = ime + " " + prezime;
			}
		}
	} while (Zeljko::korisnik.empty());
	int izlazniCvor;
	do
	{
		std::cout << "\nID naplatnog cvora?: ";
		std::cin >> izlazniCvor;
	} while (izlazniCvor < 1 || izlazniCvor > 7);
	std::system("CLS");
	while (true)
		Dusko::obracunaj_troskove(izlazniCvor);
}
