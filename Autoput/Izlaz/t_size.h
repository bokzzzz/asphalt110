#pragma once
#include<chrono>
#include<iostream>
#define DAY_SECONDS 86400
int isLeapYear(int year);
long long numOfSecondsInAllMonthsCurrentYear(int, int, int);
time_t to_t_size(std::tm* time_tm)
{
	long long seconds = 0;
	int numOfLeap = 0;
	int year = time_tm->tm_year - 1;
	int months = time_tm->tm_mon;
	int days = time_tm->tm_mday;
	int hours = time_tm->tm_hour;
	int mins = time_tm->tm_min;
	int secs = time_tm->tm_sec;
	for (int i = 1970; i <= year; i++)
	{
		if (isLeapYear(i))
			numOfLeap++;
	}
	seconds += numOfLeap * 366 * DAY_SECONDS + (year - 1969 - numOfLeap) * 365 * DAY_SECONDS;
	seconds += numOfSecondsInAllMonthsCurrentYear(days, months, year + 1);
	seconds += hours * 3600 + mins * 60 + secs;
	seconds -= 3600;
	return seconds;


}

int isLeapYear(int year)
{
	if ((year % 4 == 0) && !(year % 100 == 0))
		return 1;
	else if (year % 400 == 0)
		return 1;
	return 0;
}
long long numOfSecondsInAllMonthsCurrentYear(int days, int months, int years)
{
	long long seconds = 0;
	for (int i = 1; i < months; i++)
	{
		if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12)
			seconds += 31 * DAY_SECONDS;
		else if (i == 2)
		{
			if (isLeapYear(years))
				seconds += 29 * DAY_SECONDS;
			else
				seconds += 28 * DAY_SECONDS;
		}
		else
			seconds += 30 * DAY_SECONDS;
	}
	return seconds + (days - 1)*DAY_SECONDS;
}
