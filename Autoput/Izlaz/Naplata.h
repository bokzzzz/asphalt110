#pragma once
#include "Racun.h"
#include <vector>
#include <fstream>
#include <string>
#include <tuple>

namespace Zeljko
{
	class Naplata
	{
		std::string cvor_s; // Ime ulazne datoteke
		std::ifstream cvor; // Ulazni stream za ucitavanje potvrde
		Racun racun; // Objekat klase Racun
		float cijena; // Cijena za koristenu dionicu
		std::vector<std::tuple<char, int, int, float>> cijene; // Vektor cijena
	public:
		Naplata();
		void izvrsiNaplatu(int izlaz, int broj_ul_p);
	};
}
