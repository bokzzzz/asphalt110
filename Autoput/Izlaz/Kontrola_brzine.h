#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <chrono>
#include <iostream>
#include <string>
#include <fstream>
#include <tuple>
#include <iomanip>

namespace Dusko
{
	std::tuple<bool, int> prekoracenje_brzine(int izlazni_cvor, int broj_ul_p);
}