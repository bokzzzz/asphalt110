#include "Kontrola_brzine.h"
#include "t_size.h"

std::tuple<bool, int> Dusko::prekoracenje_brzine(int izlazni_cvor, int broj_ul_p)
{
	int ulazni_cvor;
	std::string kategorija;
	std::time_t vrijeme_ulaska;
	std::fstream ul_p(std::to_string(broj_ul_p) + ".txt", std::ios_base::in); // otvaranje ulazne potvrde
	if (ul_p.is_open() == false) throw "Nije pronadjena ulazna potvrda.";
	ul_p.ignore(150, ':');
	while (std::to_string(ul_p.get()) == " ");
	ul_p.seekg(-1, std::ios::cur);
	ul_p >> kategorija;        // ucitavanje kategorije
	ul_p.ignore(50, ':');
	while (std::to_string(ul_p.get()) == " ");
	ul_p.seekg(-1, std::ios::cur);
	ul_p >> ulazni_cvor;         // ucitavanje cvora na kome se vozilo ukljucilo na autoput
	ul_p.ignore(23);
	std::tm *vrijeme_tm = new std::tm;           // pomocna struktura za ucitavanje datuma ulaska na autoput
	ul_p >> vrijeme_tm->tm_mday;
	ul_p.get();
	ul_p >> vrijeme_tm->tm_mon;
	ul_p.get();
	ul_p >> vrijeme_tm->tm_year;
	vrijeme_tm->tm_year += 2000;
	ul_p.ignore(7);
	ul_p >> vrijeme_tm->tm_hour;
	ul_p.get();
	ul_p >> vrijeme_tm->tm_min;
	ul_p.get();
	ul_p >> vrijeme_tm->tm_sec;
	vrijeme_ulaska = to_t_size(vrijeme_tm);
	std::fstream dat("config" + kategorija + ".txt", std::ios_base::in); // datoteka sa minimalnim vremenima za odredjene dionice i kategorije
	if (ul_p.is_open() == false) throw "Datoteka nije pronadjena";
	int kraj = 0; // pomocna varijabla za kraj pretrazivanja dionice u datoteci
	int min_vrijeme;
	do
	{
		char niz[10];
		int ulazni;
		dat >> ulazni;	// trazenje ulaznog cvora u datoteci
		if (ulazni != ulazni_cvor) dat.getline(niz, 10);
		else
		{
			int izlazni; // ako je ulazni pronadjen trazi se odgovarajuci izlazni
			dat.get();
			dat >> izlazni;
			if (izlazni == izlazni_cvor)
			{
				dat.get();
				dat >> min_vrijeme; // ako je pronadjena dionica  ucitava se minimalno vrijeme u sekundama
				kraj = 1;
			}
			else dat.getline(niz, 10);
		}


	} while (!kraj);
	std::time_t razlika_vremena; // razlika trenutnog vremena(izlaska) i vremena ulaska
	razlika_vremena = std::time(NULL) - vrijeme_ulaska;
	if (razlika_vremena < min_vrijeme)
	{
		int kazna;
		std::cout << "Brzina prekoracena" << std::endl;
		if (!((min_vrijeme - razlika_vremena) / 50)) kazna = 50; // ako je prekoracenje od 1 do 10 sekundi kazna je 50;
		else kazna = 50 * ((min_vrijeme - razlika_vremena) / 50);
		return std::tuple<bool, int>(1, kazna);
	}
	std::cout << "Brzina nije prekoracena" << std::endl;

	return std::tuple<bool, int>(0, 0);
}
