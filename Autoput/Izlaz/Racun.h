#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <chrono>

namespace Zeljko
{
	extern char kategorija;
	extern std::string korisnik; // Radnik u naplatnoj kucici

	class Racun
	{
		float gotovina;
		std::time_t datum_t; // Pomocna varijabla za dohvatanje trenutnog vremena
		std::tm* datum;
		char upis[20]; // Naziv datoteke za upis
		std::ofstream dout; // Izlazni stream
	public:
		void izdaj_Racun(int a, int b, float cijena);
	};
}
