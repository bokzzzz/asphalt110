#include "Racun.h"

#define LineBreak "===================\n"

namespace Zeljko
{
	char kategorija;
	std::string korisnik; // Radnik u naplatnoj kucici


	void Zeljko::Racun::izdaj_Racun(int a, int b, float cijena)
	{
		datum_t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()); // Trenutno vrijeme
		std::cout << "Iznos za uplatu: " << cijena << "\nUplaceno: ";
		do {
			std::cin >> gotovina;
			if (gotovina < cijena)
			{
				std::cout << "Nedovoljan iznos! Pokusajte ponovo: ";
				continue;
			}
			break;
		} while (true);
		datum = std::localtime(&datum_t); // Lokalno vrijeme
		std::strftime(upis, 20, "%y%m%d%H.txt", datum);
		dout.open(std::string("Racuni/") + upis, std::ios::app);
		dout << LineBreak << "    Asphalt 110\n" << LineBreak;
		dout << std::right;
		dout << std::put_time(datum, "           %d.%m.%y\n           %T\n") << LineBreak;
		dout << kategorija << " (" << a << '-' << b << ")\n" << LineBreak; // Ispis izvrsene usluge
		dout << std::setprecision(2) << std::fixed;
		dout << "Cijena:";
		dout.width(12); dout << cijena << "\nGotovina:";
		dout.width(10); dout << gotovina << "\nPovrat:";
		dout.width(12); dout << gotovina - cijena << '\n' << LineBreak;
		dout << "Naplatu izvrsio:\n";
		dout.width(19); dout << korisnik; // Ispis izvrsioca naplate
		dout << '\n' << LineBreak << "###################\n";
		dout.close();
		std::cout << "\nNaplata uspjesno izvrsena.\n\tPritisnite <Enter> da nastavite.";
		std::cin.ignore(2);
		system("CLS");
	}
}
