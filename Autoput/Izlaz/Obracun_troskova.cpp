#include "Obracun_troskova.h"
#include "Kontrola_brzine.h"
#include "Naplata.h"

using namespace Zeljko;

void Dusko::obracunaj_troskove(int izlazni_cvor)
{
	int broj_ul_p;
	std::cout << "Broj ulazne potvrde: ";
	std::cin >> broj_ul_p;
	std::tuple<bool, int> kazna;  // informacije o kazni (da li je brzina prekoracena i iznos kazne)
	try
	{
		kazna = Dusko::prekoracenje_brzine(izlazni_cvor, broj_ul_p);
		if (std::get<0>(kazna) == true)
		{
			std::cout << "Kazna iznosi: " << std::get<1>(kazna);
		}
	}
	catch (const char*)
	{
		std::exit(1);
	}
	static Naplata naplata_putarine;
	naplata_putarine.izvrsiNaplatu(izlazni_cvor, broj_ul_p);
}
